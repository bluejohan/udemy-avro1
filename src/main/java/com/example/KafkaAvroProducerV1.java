package com.example;

import java.util.Properties;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import io.confluent.kafka.serializers.KafkaAvroSerializer;

public class KafkaAvroProducerV1 {

    public static void main(String[] args) {
        Properties properties = new Properties();

        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(ProducerConfig.ACKS_CONFIG, "1");
        properties.put(ProducerConfig.RETRIES_CONFIG, "10");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class.getName());
        properties.put("schema.registry.url", "http://localhost:8081");


        KafkaProducer<String, CustomerX> kafkaProducer = new KafkaProducer<String, CustomerX>(properties);

        String topic = "zedbpih.customer-avro";

        CustomerX customer;
        customer = new CustomerX.Builder()
                .setAge(34)
                .setAutomatedEmail(false)
                .setFirstName("Piotr")
                .setLastName("Hajkowski")
                .setHeight(183f)
                .setWeight(75.5f)
                .build();

		ProducerRecord<String, CustomerX> record = new ProducerRecord<>(topic, customer);

		kafkaProducer.send(record, new Callback() {
            @Override
            public void onCompletion(RecordMetadata recordMetadata, Exception e) {
                if (e == null) {
                    System.out.println("Success!");
                    System.out.println(recordMetadata.toString());
                }
                else {
                    e.printStackTrace();

                }
            }

        });

        kafkaProducer.flush();
        kafkaProducer.close();

    }

}
